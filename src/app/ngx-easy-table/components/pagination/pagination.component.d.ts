import { EventEmitter } from '@angular/core';
export declare class PaginationComponent {
    pagination: any;
    id: any;
    updateRange: EventEmitter<{}>;
    ranges: number[];
    limit: number;
    showRange: boolean;
    onPageChange($event: any): void;
    changeLimit(limit: any): void;
}
